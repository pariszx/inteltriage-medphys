import { app } from './app';

const start = async () => {
  console.log('Starting...');

  // if (!process.argv[2]) {
  //   throw new Error('JWT_KEY must be defined');
  // }

  app.listen(3000, () => {
    console.log('Listening on port 3000');
  });
};

start();
