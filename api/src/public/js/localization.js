
const localization = async () => {

  document.querySelector('#no-query-yet').style.display = 'none';
  document.querySelector('#heatmap').style.display = 'none';
  document.querySelector('#no-data-found').style.display = 'none';
  
  const from = document.getElementById('from').value;
  const to = document.getElementById('to').value;

  document.querySelector('#loading').style.display = 'block';

  const data = await getData(from, to);
  
  document.querySelector('#loading').style.display = 'none';

  const events = data.result;

  if (events.length > 0) {

    createHeatmap(events);

    createTreemap(events);

    createWaitingTimesTable(events);
    
    document.querySelector('#heatmap').style.display = 'block';
  } else {
    document.querySelector('#no-data-found').style.display = 'block';
  }
};

const getData = async (from, to) => {
  var myHeaders = new Headers();
  myHeaders.append("Cookie", "PHPSESSID=0qcdot5djb25fu87ekmvqqfg3m");

  var formdata = new FormData();
  formdata.append("secret", "Sd&*z#89A12");
  formdata.append("from_date", from);
  formdata.append("to_date", to);
  formdata.append("mac_address", "F4CFA2496FC9");

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: formdata,
    redirect: 'follow'
  };

  return fetch("https://dev2.vida24.com/api/activity_tracking", requestOptions).then(response => response.json());
};

const preprocess = (events) => {
  // preprocess attribute 'SSID_MAC_ADDRESS'
  events.forEach(event => {
    // remove /n from the end of the string of events attribute 'SSID_MAC_ADDRESS'
    event.SSID_MAC_ADDRESS = event.SSID_MAC_ADDRESS.replace(/\n/g, '');

    // remove : from the attributes 'SSID_MAC_ADDRESS'
    event.SSID_MAC_ADDRESS = event.SSID_MAC_ADDRESS.replace(/:/g, '');

    // make all the attributes upper case
    event.SSID_MAC_ADDRESS = event.SSID_MAC_ADDRESS.toUpperCase();

    // keep only date from 'created_at' ISO datetime
    event.created_at = event.created_at.split('T')[0];
  
  });
  return events;
};

const group = (events, attribute) => {
  // group events based on attribute
  const groupEvents = events.reduce((acc, event) => {
    if (!acc[event[attribute]]) {
      acc[event[attribute]] = [];
    }
    acc[event[attribute]].push(event);
    return acc;
  }, {});

  return groupEvents;
};

const count = (events, attribute) => {
  // count events based on attribute
  const countEvents = events.reduce((acc, event) => {
    if (acc[event[attribute]]) {
      acc[event[attribute]]++;
    }
    else {
      acc[event[attribute]] = 1;
    }
    return acc;
  }, {});

  return countEvents;
};

const distinct = (events, attribute) => {
  // distinct events based on attribute
  const distinctEvents = events.reduce((acc, event) => {
    if (!acc[event[attribute]]) {
      acc[event[attribute]] = true;
    }
    return acc;
  }, {});

  // convert distinctEvents to an array
  const distinctEventsArray = Object.keys(distinctEvents);

  return distinctEventsArray;
};

const sort = (events, attribute, order) => {
  // sort events based on attribute and order ('asc', 'desc')
  const sortedEvents = events.sort((a, b) => {
    if (order === 'asc') {
      return a[attribute] > b[attribute] ? 1 : -1;
    } else {
      return a[attribute] < b[attribute] ? 1 : -1;
    }
  });

  return sortedEvents;
};

const findHotspotName = (hotspot) => {
  let name = '';

  // make a switch/case to get the name of the hotspot
  switch (hotspot) {
    case '14169D50A820':
      name = 'IntelTriage_A (ΧΕΙΡΟΥΡΓΙΚΟ)';
      break;
    case '14169D50A680':
      name = 'IntelTriage_B (ΔΙΑΔΡΟΜΟΣ ΑΞΟΝΙΚΟΥ)';
      break;
    case '14169D50A6C0':
      name = 'IntelTriage_C (ΝΕΥΡΟΛΟΓΙΚΟ)';
      break;
    case '14169D508720':
      name = 'IntelTriage_D (ΠΑΘΟΛΟΓΙΚΟ 1)';
      break;
    case '14169D4B0000':
      name = 'IntelTriage_E (ΠΑΘΟΛΟΓΙΚΟ 2)';
      break;
    case '14169D509880':
      name = 'IntelTriage_F (ΒΡΑΧΕΙΑ ΝΟΣΗΛΕΙΑ)';
      break;
    case '14169D509940':
      name = 'IntelTriage_G (SHOCK ROOM)';
      break;
    case '14169D4B07E0':
      name = 'IntelTriage_H (ΔΙΑΔΡΟΜΟΣ WC)';
      break;
    case '14169D50B820':
      name = 'IntelTriage_I (ΑΙΜΟΛΗΨΙΕΣ)';
      break;
    case '14169D509C60':
      name = 'IntelTriage_J (ΚΑΡΔΙΟΛΟΓΙΚΟ)';
      break;
    default:
      name = 'Unknown (' + hotspot + ')';
      break;
  }

  return name;
};

const createHeatmap = (events) => {
  events = preprocess(events);

  const dates = distinct(events, 'created_at');

  const groupEventsPerHotspot = group(events, 'SSID_MAC_ADDRESS');

  // for each of the hotspots, get the count of the events per date
  let countEventsPerHotspotPerDate = Object.keys(groupEventsPerHotspot).map(key => {
    const events = groupEventsPerHotspot[key];
    const countEventsPerDate = count(events, 'created_at');

    // Convert object to array, and each object of the array has a 'x' attribute that is a date and a 'y' attribute that is the count of the events. There should be an entry for all distinct dates, and if there is no count for that date, there should be an entry with a 'y' value of 0.
    const countEventsPerDateArray = [];
    for (date of dates) {
      if (countEventsPerDate[date]) {
        countEventsPerDateArray.push({
          x: date,
          y: countEventsPerDate[date]
        });
      } else {
        countEventsPerDateArray.push({
          x: date,
          y: 0
        });
      }
    }

    // find name of hotspot
    const name = findHotspotName(key);

    return {
      name: name,
      data: countEventsPerDateArray
    }
  });

  countEventsPerHotspotPerDate = sort(countEventsPerHotspotPerDate, 'name', 'desc');

  refreshHeatmapChart(countEventsPerHotspotPerDate);
};

const refreshHeatmapChart = (data) => {

  const from = document.getElementById('from').value;
  const to = document.getElementById('to').value;

  var options = {
    series: data,
    chart: {
      height: 350,
      type: 'heatmap',
    },
    dataLabels: {
      enabled: true
    },
    colors: ["#008FFB"],
    title: {
      text: 'HeatMap των Hotspots από ' + new Date(from).toLocaleDateString() + ' έως ' + new Date(to).toLocaleDateString()
    },
  };

  document.querySelector("#heatmap-chart").remove();
  const chartDiv = document.createElement('div');
  chartDiv.setAttribute('id', 'heatmap-chart');
  document.querySelector('#chart-parent').append(chartDiv);
  var chart = new ApexCharts(document.querySelector('#heatmap-chart'), options);
  chart.render();
};

const createTreemap = (events) => {

  events = preprocess(events);

  const groupEventsPerHotspot = count(events, 'SSID_MAC_ADDRESS');

  // convert groupEventsPerHotspot to an array
  const groupEventsPerHotspotArray = Object.keys(groupEventsPerHotspot).map(key => {
    return {
      x: findHotspotName(key),
      y: groupEventsPerHotspot[key]
    };
  });

  refreshTreemapChart(groupEventsPerHotspotArray);
};

const refreshTreemapChart = (data) => {

  var options = {
    series: [
      {
        data: data,
      }
    ],
    chart: {
      height: 350,
      type: 'treemap',
    },
    dataLabels: {
      enabled: true
    },
    colors: ["#008FFB"],
    title: {
      text: 'Treemap των Hotspots'
    },
  };

  document.querySelector("#treemap-chart").remove();
  const chartDiv = document.createElement('div');
  chartDiv.setAttribute('id', 'treemap-chart');
  document.querySelector('#chart-parent').append(chartDiv);
  var chart = new ApexCharts(document.querySelector('#treemap-chart'), options);
  chart.render();
};

const createWaitingTimesTable = (events) => {

  events = sort(events, 'updated_at', 'asc'); 

  const hotspotsArray = distinct(events, 'SSID_MAC_ADDRESS');

  // convert from array to object
  const hotspotsObject = {};

  for (hotspot of hotspotsArray) {
    hotspotsObject[hotspot] = [];
  }

  let start = new Date(events[0].updated_at);
  let currentSSID = events[0].SSID_MAC_ADDRESS;
  let end;
  for (event of events) {
    if (event.SSID_MAC_ADDRESS === currentSSID) {
      const durationSoFar = (new Date(event.updated_at) - start) / 1000 / 60;
      if (durationSoFar > 1440) { // day changed
        const duration = (end - start) / 1000 / 60;
        if (duration > 0) {
          hotspotsObject[currentSSID].push({
            start: start,
            end: end,
            duration: duration
          });
        }
        start = new Date(event.updated_at);
      }
      end = new Date(event.updated_at);
    } else {
      const duration = (end - start) / 1000 / 60;
      if (duration > 0) {
        hotspotsObject[currentSSID].push({
          start: start,
          end: end,
          duration: duration
        });
      }
      start = new Date(event.updated_at);
      end = new Date(event.updated_at);
      currentSSID = event.SSID_MAC_ADDRESS;
    }
  }

  // convert from object to array
  let hotspotsDataForTable = [];
  for (hotspot of hotspotsArray) {
    
    // calculate mean waiting time and find max/min waiting time
    let max = 0;
    let min = 0;
    let sum = 0;
    for (waitingTime of hotspotsObject[hotspot]) {
      if (waitingTime.duration > max) {
        max = waitingTime.duration;
      }
      if (waitingTime.duration < min || min === 0) {
        min = waitingTime.duration;
      }
      sum += waitingTime.duration;
    }
    const mean = sum / hotspotsObject[hotspot].length || 0;

    hotspotsDataForTable.push({
      name: findHotspotName(hotspot),
      data: {
        mean: mean.toFixed(2),
        max: max.toFixed(2),
        min: min.toFixed(2)
      }
    });

    hotspotsDataForTable = sort(hotspotsDataForTable, 'name', 'asc');

    // create html table rows and columns as string in variable 'tableHtmlString' from 'hotspotsDataForTable' array
    const tableHtmlString = hotspotsDataForTable.map(hotspot => {
      return `<tr>
                <td>${hotspot.name}</td>
                <td>${hotspot.data.mean}</td>
                <td>${hotspot.data.max}</td>
              </tr>`;
    }
    ).join('');

    // insert html table rows and columns into html table
    document.querySelector('#waiting-times-table').innerHTML = tableHtmlString;

  }

};

document.querySelector('#no-data-found').style.display = 'none';
document.querySelector('#heatmap').style.display = 'none';
document.querySelector('#loading').style.display = 'none';