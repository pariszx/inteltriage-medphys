import sqlite3 from 'sqlite3';

const DBSOURCE = 'db.sqlite';

const db = new sqlite3.Database(DBSOURCE, (err: any) => {
  if (err) {
    // Cannot open database
    console.error(err.message);
    throw err;
  } else {
    console.log('Connected to the SQLite database.');

    // Create table users
    db.run(`CREATE TABLE users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            email text UNIQUE, 
            password text, 
            CONSTRAINT email_unique UNIQUE (email)
            )`,
      (err: any) => {
        if (err) {
          // Table already created
          console.log('table already created');
        } else {
          // Table just created, creating some rows
        }
      });

    // Create table settings
    db.run(`CREATE TABLE settings (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            variable TEXT,
            baseline FLOAT,
            upperLimit FLOAT,
            lowerLimit FLOAT,
            allowedDeviationPercentage FLOAT,
            numOfConsecutiveMeasurements FLOAT,
            patientId TEXT
            )`,
      (err: any) => {
        if (err) {
          // Table already created
          console.log('table already created');
        } else {
          // Table just created, creating some rows
        }
      });

    // Create table measurements
    db.run(`CREATE TABLE measurements (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            variable TEXT,
            value FLOAT,
            patientId TEXT,
            timestamp DATETIME
            )`,
      (err: any) => {
        if (err) {
          // Table already created
          console.log('table already created');
        } else {
          // Table just created, creating some rows
        }
      });
  }
});

export { db };