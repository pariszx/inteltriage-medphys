import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import { dssRouter } from './routes/dss';
import { alertRouter } from './routes/alerting';
import { signinRouter } from './routes/signin';
import { signupRouter } from './routes/signup';
import { errorHandler } from './middlewares/error-handler';
import { currentUserRouter } from './routes/current-user';
import cookieSession from 'cookie-session';
import { signoutRouter } from './routes/signout';
import { spo2FhirRouter } from './routes/fhir/spo2';
import { respiratoryRateFhirRouter } from './routes/fhir/respiratory-rate';
import { heartRateFhirRouter } from './routes/fhir/heart-rate';

const path = require('path')

const app = express();
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: false
  })
);

app.use(dssRouter);
app.use(alertRouter);
app.use(spo2FhirRouter);
app.use(respiratoryRateFhirRouter);
app.use(heartRateFhirRouter);
app.use(signupRouter);
app.use(signinRouter);
app.use(signoutRouter);
app.use(currentUserRouter);

app.use('/localization', express.static(path.join(__dirname, 'public')));

app.all('*', async (req, res) => {
  res.status(404).json({ 'error': 'Not found' });
  return;
});

app.use(errorHandler);

export { app };
