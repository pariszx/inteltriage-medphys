import express, { Request, Response } from 'express';
import { db } from '../database';
import { body } from 'express-validator';
import { validateRequest } from '../middlewares/validate-request';
import { BadRequestError } from '../errors/bad-request-error';

const router = express.Router();

router.get('/api/settings', async (req: Request, res: Response) => {
  const sql = 'select * from settings';
  const params: any[] = [];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ 'error': err.message });
      return;
    }
    if (rows) {
      res.status(200).json({ 'status': 'OK', 'data': rows });
    }
  });
});

router.get('/api/settings/:id', async (req: Request, res: Response) => {
  const sql = 'select * from settings where patientId = ?';
  const params: any[] = [req.params.id];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ 'error': err.message });
      return;
    }
    if (rows) {
      res.status(200).json({ 'status': 'OK', 'data': rows });
    }
  });
});


router.post('/api/settings',
  [
    body('variable').isIn(['spo2', 'heart-rate', 'breathing-rate']).withMessage('variable must be one of the following: spo2, heart-rate, breathing-rate'),
    body('patientId').notEmpty().withMessage('patientId must be specified')
  ],
  validateRequest,
async (req: Request, res: Response) => {
  var input = [
    req.body.variable,
    req.body.patientId,
    req.body.baseline,
    req.body.upperLimit,
    req.body.lowerLimit,  
    req.body.allowedDeviationPercentage,
    req.body.numOfConsecutiveMeasurements
  ]

  const sql = 'select * from settings where variable = ? and patientId = ?';
  const params: any[] = [input[0], input[1]];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ 'error': err.message });
      return;
    }
    if (rows) {
      if (rows.length > 0) {
        const sql = 'UPDATE settings set variable = ?, patientId = ?, baseline = ?, upperLimit = ?, lowerLimit = ?, allowedDeviationPercentage = ?, numOfConsecutiveMeasurements = ? WHERE id = ?';
        const params: any[] = [input[0], input[1], input[2], input[3], input[4], input[5], input[6], rows[0].id];
        db.run(sql, params, function (err: any) {
          if (err) {
            res.status(400).json({ 'error': err.message });
            return;
          }
          res.status(201).json({ 'status': 'OK', 'data': params, changes: this.changes });
        });
      } else {
        const sql = 'INSERT INTO settings (variable, patientId, baseline, upperLimit, lowerLimit, allowedDeviationPercentage, numOfConsecutiveMeasurements) VALUES (?,?,?,?,?,?,?)';
        const params: any[] = input;
        db.run(sql, params, function (err: any) {
          if (err) {
            res.status(400).json({ 'error': err.message });
            return;
          }
          res.status(201).json({
            message: 'success',
            data: params,
            id: this.lastID
          })
        });
      }
    }
  });
});

router.get('/api/measurements', async (req: Request, res: Response) => {
  const patientId = req.query.patientId;

  let sql = ''; const params: any[] = [];

  if (patientId) {
    sql = 'SELECT * from measurements WHERE patientId = ?';
    params.push(req.query.patientId);
  } else {
    sql = 'SELECT * from measurements';
  }

  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ 'error': err.message });
      return;
    }
    if (rows) {
      res.status(200).json({ 'status': 'OK', 'data': rows });
    }
  });
});

router.post('/api/measurements',
  [
    body('variable').isIn(['spo2', 'heart-rate', 'breathing-rate']).withMessage('variable must be one of the following: spo2, heart-rate, breathing-rate'),
    body('patientId').notEmpty().withMessage('patientId must be specified'),
    body('value').notEmpty().withMessage('value must be specified'),
    body('timestamp').notEmpty().withMessage('value must be specified')
  ],
  validateRequest,
async (req: Request, res: Response) => {
  var input = [
    req.body.variable,
    req.body.value,
    req.body.patientId,
    req.body.timestamp
  ];

  // this is the alert for outside lower-upper lims
  const type1Alert = {
    alert: false,
    from: null,
    to: null,
    variable: null,
    description: ''
  };
  let type1AlertCount = 0;
  let moreThanUpperLimit = false;
  let lessThanLowerLimit = false;

  // this is the alert for outside diviation percentage from baseline
  const type2Alert = {
    alert: false,
    from: null,
    to: null,
    variable: null,
    description: ''
  };
  let type2AlertCount = 0;

  let sql = 'INSERT INTO measurements (variable, value, patientId, timestamp) VALUES (?, ?, ?, ?)';
  const params: any[] = [input[0], input[1], input[2], input[3]];
  db.run(sql, params, function (err: any) {
    if (err) {
      res.status(400).json({ 'error': err.message });
      return;
    }

    sql = 'SELECT * FROM settings where variable = ? and patientId = ?';
    const params: any[] = [input[0], input[2]];
    db.all(sql, params, (err, settingsRows) => {
      if (settingsRows.length > 0) {
        const settings = settingsRows[0];

        sql = 'SELECT * FROM measurements WHERE patientId = ? AND variable = ? ORDER BY timestamp DESC';
        const params: any[] = [input[2], input[0]];
        db.all(sql, params, (err, measurementsRows) => {
          if (measurementsRows) {
            const measurements = measurementsRows;

            for (let i = 0; i < settings.numOfConsecutiveMeasurements; i++) {
              if (measurements[i]) {

                console.log(`${new Date().toISOString()} checking measurment`, measurements[i]);

                // check if the measurement is outside the lower-upper lims
                if (measurements[i].value > settings.upperLimit || measurements[i].value < settings.lowerLimit) {
                  type1AlertCount++;
                  console.log('measurment outside lower-upper lims', type1AlertCount);
                  if (measurements[i].value > settings.upperLimit) {
                    moreThanUpperLimit = true;
                  }
                  if (measurements[i].value < settings.lowerLimit) {
                    lessThanLowerLimit = true;
                  }
                }
                if (type1AlertCount >= settings.numOfConsecutiveMeasurements) {
                  type1Alert.alert = true;
                  type1Alert.from = measurements[i].timestamp;
                  type1Alert.to = measurements[i - settings.numOfConsecutiveMeasurements + 1].timestamp;
                  type1Alert.variable = measurements[i].variable;
                  type1Alert.description = 'Outside lower-upper limit';
                }

                // check if the measurement is outside the diviation percentage from baseline
                if (measurements[i].value > settings.baseline + (settings.allowedDeviationPercentage * settings.baseline) / 100 || measurements[i].value < settings.baseline - (settings.allowedDeviationPercentage * settings.baseline) / 100) {
                  type2AlertCount++;
                  console.log('measurment outside diviation percentage from baseline', type2AlertCount);
                }
                if (type2AlertCount >= settings.numOfConsecutiveMeasurements) {
                  type2Alert.alert = true;
                  type2Alert.from = measurements[i].timestamp;
                  type2Alert.to = measurements[i - settings.numOfConsecutiveMeasurements + 1].timestamp;
                  type2Alert.variable = measurements[i].variable;
                  type2Alert.description = 'Outside allowed deviation percentage';
                }
              }
            }
          }
          const alerts = [];
          if (type1Alert.alert) {
            if (moreThanUpperLimit && !lessThanLowerLimit) {
              type1Alert.description = 'Measurements higher than the upper limit';
            }
            if (lessThanLowerLimit && !moreThanUpperLimit) {
              type1Alert.description = 'Measurements lower than the lower limit';
            }

            alerts.push(type1Alert);
          }
          if (type2Alert.alert) {
            // alerts.push(type2Alert);
          }

          res.status(201).json({ 'status': 'OK', 'data': params, changes: this.changes, alerts: alerts });
        });
      } else {
        res.status(400).json({
          'status': 'error',
          'message': 'No settings found for this patient, please add settings before you add measurements'
        });
      }
    });
  });
});

export { router as alertRouter };
