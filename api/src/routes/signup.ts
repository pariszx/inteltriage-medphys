import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import jwt from 'jsonwebtoken';
import { db } from '../database';
import { BadRequestError } from '../errors/bad-request-error';
import { validateRequest } from '../middlewares/validate-request';
import { Password } from '../services/password';

const router = express.Router();

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { email, password } = req.body;
  
    const hashedPassword = await Password.toHash(password);

    const sql = 'INSERT INTO users (email, password) VALUES (?,?)';
    const params: any[] = [email, hashedPassword];
    db.run(sql, params, (err) => {
      if (err) {
        throw new BadRequestError(err.message);
        return;
      }
      res.status(201).send({ message: 'User created' });
    });
  }
);

export { router as signupRouter };
