import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import jwt from 'jsonwebtoken';
import { BadRequestError } from '../errors/bad-request-error';
import { validateRequest } from '../middlewares/validate-request';
import { db } from '../database';
import { Password } from '../services/password';

const router = express.Router();

router.post(
  '/api/users/signin',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .notEmpty()
      .withMessage('You must supply a password'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { email, password } = req.body;
    
    let existingUser: any = null;

    const sql = 'select * from users where email = ?';
    const params = [email];
    db.all(sql, params, async (err, rows) => {
      if (err) {
        res.status(400).send('Invalid credentials');
        return;
      }
      if (rows) {
        existingUser = rows[0];
      }

      if (!existingUser) {
        res.status(400).send('Invalid credentials');
        return;
      }

      const passwordsMatch = await Password.compare(
        existingUser.password,
        password
      );
      if (!passwordsMatch) {
        res.status(400).send('Invalid credentials');
        return;
      }

      // Generate JWT
      const userJwt = jwt.sign(
        {
          id: existingUser.id,
          email: existingUser.email,
        },
        'process.argv[2]'
      );

      // Store it on session object
      console.log(req.session);
      req.session = {
        jwt: userJwt,
      };

      res.status(200).send(existingUser);
    });
  }
);

export { router as signinRouter };
