import express, { Request, Response } from 'express';
import { db } from '../../database';
import { body } from 'express-validator';
import { validateRequest } from '../../middlewares/validate-request';

const router = express.Router();

router.post('/api/fhir/respiratory-rate',
  [
    body('value').isFloat().withMessage('value must be a number'),
    body('timestamp').custom((value) => new Date(value)).withMessage('timestamp must be a valid date'),
    body('patientId').notEmpty().withMessage('patientId must be specified')
  ],
  validateRequest,
  async (req: Request, res: Response) => {

    const body = {
      "resourceType": "Observation",
      "id": "respiratory-rate",
      "meta": {
        "profile": [
          "http://hl7.org/fhir/StructureDefinition/vitalsigns"
        ]
      },
      "text": {
        "status": "generated",
        "div": `<div xmlns=\"http://www.w3.org/1999/xhtml\"><p><b>Generated Narrative with Details</b></p><p><b>id</b>: respiratory-rate</p><p><b>meta</b>: </p><p><b>status</b>: final</p><p><b>category</b>: Vital Signs <span>(Details : {http://terminology.hl7.org/CodeSystem/observation-category code 'vital-signs' = 'Vital Signs', given as 'Vital Signs'})</span></p><p><b>code</b>: Respiratory rate <span>(Details : {LOINC code '9279-1' = 'Respiratory rate', given as 'Respiratory rate'})</span></p><p><b>subject</b>: <a>Patient/${req.body.patientId}</a></p><p><b>effective</b>: ${new Date(req.body.timestamp).toISOString()}</p><p><b>value</b>: ${req.body.value} breaths/minute<span> (Details: UCUM code /min = '/min')</span></p></div>`
      },
      "status": "final",
      "category": [
        {
          "coding": [
            {
              "system": "http://terminology.hl7.org/CodeSystem/observation-category",
              "code": "vital-signs",
              "display": "Vital Signs"
            }
          ],
          "text": "Vital Signs"
        }
      ],
      "code": {
        "coding": [
          {
            "system": "http://loinc.org",
            "code": "9279-1",
            "display": "Respiratory rate"
          }
        ],
        "text": "Respiratory rate"
      },
      "subject": {
        "reference": "Patient/" + req.body.patientId
      },
      "effectiveDateTime": new Date(req.body.timestamp).toISOString(),
      "valueQuantity": {
        "value": req.body.value,
        "unit": "breaths/minute",
        "system": "http://unitsofmeasure.org",
        "code": "/min"
      }
    };

    res.status(200).json(body);
  });

export { router as respiratoryRateFhirRouter };
