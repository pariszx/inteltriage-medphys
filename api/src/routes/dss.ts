import express, { Request, Response } from 'express';
import amqp from 'amqplib';
import { BadRequestError } from '../errors/bad-request-error';
import { validateRequest } from '../middlewares/validate-request';
import { body } from 'express-validator';

const router = express.Router();

router.post('/api/dss',
  [
    body('peptikou_katigories').isArray().withMessage('peptikou_katigories must be an array'),
    body('genika_kai_akathorista_kathgories').isArray().withMessage('genika_kai_akathorista_kathgories must be an array'),
    body('miskeletikou_katigories').isArray().withMessage('miskeletikou_katigories must be an array'),
    body('neurologika_kathgories').isArray().withMessage('neurologika_kathgories must be an array'),
    body('anapneustikou_katigories').isArray().withMessage('anapneustikou_katigories must be an array'),
    body('kardiaggeiakou_katigories').isArray().withMessage('kardiaggeiakou_katigories must be an array'),
    body('filo').isIn([
      'Άνδρας', 'Γυναίκα'
    ]).withMessage('filo must be one of the following: Άνδρας, Γυναίκα'),
    body('kathgoria_afiksis').isIn([
      'Άλλο μέσο', 'Αμαξίδιο', 'Ασθενοφόρο', 'Αστυνομικό όχημα', 'Αυτοκίνητο', 'Μέσα Μαζικής Μεταφοράς', 'Περιπατητικός'
    ]).withMessage('kathgoria_afiksis must be one of the following: Άλλο μέσο, Αμαξίδιο, Ασθενοφόρο, Αστυνομικό όχημα, Αυτοκίνητο, Μέσα Μαζικής Μεταφοράς, Περιπατητικός'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {

  const final = {
    'Φύλο ασθενή': req.body.filo || null,
    'Ηλικία ασθενή': Number.parseFloat(req.body.hlikia) || null,
    'Αριθμός νοσηλειών ή επισκέψεων (κατά τον περασμένο χρόνο)': Number.parseFloat(req.body.plithos_nosilion) || null,
    'Κατηγορία άφιξης ασθενή': req.body.kathgoria_afiksis || null,
    'Καρδιακοί παλμοί (HR) (min)': Number.parseFloat(req.body.kardiakoi_palmoi) || null,
    'Συστολική αρτηριακή πίεση (mmHg)': Number.parseFloat(req.body.systoliki_artiriaki_piesi) || null,
    'Διαστολική αρτηριακή πίεση (mmg)': Number.parseFloat(req.body.diastoliki_artiriki_piesi) || null,
    'Αναπνευστικός ρυθμός (min)': Number.parseFloat(req.body.anapneustikos_rithmos) || null,
    'Κορεσμός οξυγόνου (SpO2) (%)': Number.parseFloat(req.body.koresmos_oksigonou) || null,
    'Θερμοκρασία (C)': Number.parseFloat(req.body.thermokrasia) || null, // pending validation
    '7.3.1': req.body.peptikou_katigories.includes('0') ? 1 : 0, // Γενικευμένο κοιλιακό άλγος
    '7.1.44': req.body.genika_kai_akathorista_kathgories.includes('32') ? 1 : 0, // Αλλεργία 
    '7.7.2': req.body.miskeletikou_katigories.includes('2') ? 1 : 0, // Συμπτώματα ράχης
    '7.7.4': req.body.miskeletikou_katigories.includes('6') ? 1 : 0, // Συμπτώματα στήθους
    '7.6.2': req.body.kardiaggeiakou_katigories.includes('2') ? 1 : 0, // Πίεση, σφίξιμο αποδιδόμενο στην καρδιά
    '7.1.4': req.body.genika_kai_akathorista_kathgories.includes('4') ? 1 : 0, // Γενική αδυναμία
    '7.3.11': req.body.peptikou_katigories.includes('20') ? 1 : 0, // Διάρροια
    '7.8.15': req.body.neurologika_kathgories.includes('15') ? 1 : 0, // Ίλιγγος / ζάλη
    '7.10.2': req.body.anapneustikou_katigories.includes('2') ? 1 : 0, // Ταχύπνοια / δύσπνοια
    '7.3.10': req.body.peptikou_katigories.includes('18') ? 1 : 0, // Έμετος
    '7.1.6': req.body.genika_kai_akathorista_kathgories.includes('8') ? 1 : 0, // Λιποθυμία
    '7.1.3': req.body.genika_kai_akathorista_kathgories.includes('2') ? 1 : 0, // Πυρετός
    '7.6.29': req.body.kardiaggeiakou_katigories.includes('15') ? 1 : 0, // Υψηλή αρτηριακή πίεση
    '7.6.5': req.body.kardiaggeiakou_katigories.includes('8') ? 1 : 0, // Άλλες ανωμαλίες ρυθμού
    '7.6.4': req.body.kardiaggeiakou_katigories.includes('6') ? 1 : 0 // Αίσθημα παλμών
  };

  console.log('ill try to connect to rabbitmq...');

  const conn = await amqp.connect('amqp://localhost');
  
  console.log('connected');
  
  const channel = await conn.createChannel();
    
  console.log('channel created');
  
  const inputs = 'inputs';
  const results = 'results';

  channel.assertQueue(inputs, { durable: false });
  channel.assertQueue(results, { durable: false })

  // const message = new Date().toISOString();

  channel.sendToQueue(inputs, Buffer.from(JSON.stringify(final)));

  console.log(`Message ${Buffer.from(JSON.stringify(final))} was sent`);

  channel.consume(results, (msg: any) => {
    console.log(`Message ${msg.content.toString()} was received`);
    res.json({
      'status': 'OK',
      'data': msg.content.toString(),
      'accuracy': 0.75
    });
    channel.close();
  }, { noAck: true });
});  

export { router as dssRouter };
