#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
import xgboost
from sklearn.preprocessing import LabelEncoder
import pickle
import os 

# necessary paths, filenames and variables
encoders_path = r'encoders'
model_name = r'XGBoost.json'
categorical = ['Φύλο ασθενή', 'Κατηγορία άφιξης ασθενή']

# encode sample's categorical data
def label_encoder(y, col_name):
    encoder = LabelEncoder()
    encoder.classes_ = np.load(os.path.join(encoders_path, str(col_name)+'.npy'), allow_pickle=True)
    y_enc = encoder.transform(y)
    return y_enc[0]


def prepare_data(input1):
    for col in categorical: 
        input1[col] = label_encoder(pd.Series(input1[col]), col)
    return input1

def predict(input1):  
    # encode categorical variables, same transformations used for training dataset
    input1 = prepare_data(input1)

    # replace nan values with -1
    input1 = input1.fillna(-1)
    
    # initialize classifier
    model_xgb_2 = xgboost.XGBClassifier()

    # load pretrained model
    model_xgb_2.load_model(model_name)

    # make prediction
    y_pred = model_xgb_2.predict(np.array(input1).reshape(1, input1.shape[0]))
    
    if y_pred == 0: output = 'Discharge'
    elif y_pred == 1: output = 'Admission'

    return output

