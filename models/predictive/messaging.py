from make_prediction import *
import os
import pandas as pd
import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='inputs')
channel.queue_declare(queue='results')
print(' [*] Waiting for messages. To exit press CTRL+C')

def callback(ch, method, properties, body):
  
  print('got message')

  inputs = pd.read_json(body, typ='series') 

  output = predict(inputs)

  print('my response is: ', output)

  # send a message back
  channel.basic_publish(exchange='', routing_key='results', body=output)

# receive message and complete simulation
channel.basic_consume(on_message_callback=callback, queue='inputs', auto_ack=True)
channel.start_consuming()
