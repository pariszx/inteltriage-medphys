# IntelTriage DSS - Medphys

## APIs

This is an node.js/express API that interacts with the python model through a RabbitMQ message broker.

## Models

This is the folder with the python models.

Insttall python 3.7.7 and then virtualenv, in the folder `models` with the command:
```
python3.7 -m pip install virtualenv
python3.7 -m virtualenv venv
source venv/bin/activate
```

## Installations

Install rabbitmq:

```
brew install rabbitmq
```

Install pika (python lib to interact with rabbitmq):

```
pip install pika
pip install pandas
pip install sklearn
pip install xgboost
```

Install amqplib (npm package to interact with rabbitmq):

```
npm install amqplib
```

Then start your RabbitMQ instance:

```
brew services start rabbitmq
```

To deploy the services first install pm2. Then, for the model, go to the folder models/predictive and run

```
pm2 start bashscript.sh --name model --log public.log
```

To deploy the api, go to the api foler, and run

```
pm2 start bashscript.sh --name api --log public.log
```

These commands will create a `public.log` file in both these folders. To check the logs, run

```
tail -f public.log
```